# Pointfree Programming

Passing a named function as an argument to avoid writing an anonymous function with interim variables instead. Pointfree programming increases code legibility, decreases the surface area for bugs, and makes our code more composable and unit testable.

```javascript
// Pointfree Programming
const array = [1, 2, 3];

// bad
array.map(x => x * 2);

// good
const array = [1, 2, 3];
const double = x => x * 2;
// it's possible to UT double now
array.map(double);
```
