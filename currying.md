# Currying

The act of refactoring a function so that it receives its arguments one at a time.

Arity describes the number of arguments a function receives. Depending on the number it receives, there are specific words to describe these functions.

A function that receives one is called a unary function. A function that receives two arguments is called a binary, three equals a ternary, and four equals a quaternary, so forth and so on. Thus the act of currying can be described as taking a multivariate function and turning it into a series of unary functions.

```javascript
// function
const add = (a, b) => a + b;

// curried function
const addCurried = x => y => x + y;

const addThree = addCurried(3);
console.log(addThree(4));
// 7
console.log(addThree(6));
// 9
```
