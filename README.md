# Fonctionnal Programming

**Concepts :**

- higher order functions 👌
- pure and impure functions 👌
- immutable data structures 👌
- currying 👌
- partial application 👌
- the importance of argument order 👌
- pointfree programming 👌
- composition 👌
- how to debug functional compositions 👌
