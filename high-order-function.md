# High Order Function

1. Accepts a function as an argument
2. return a new function

```javascript
const withCount = fn => {
  let count = 0;

  return (...args) => {
    console.log(`Call count: ${++count}`);
    return fn(...args);
  };
};

const add = (x, y) => x + y;

const countedAdd = withCount(add);


console.log(countedAdd(1, 2));
// Call count : 1
console.log(countedAdd(2, 2));
// Call count : 2
console.log(countedAdd(3, 2));
// Call count : 3
```

withCount use a closure with count variable. this function when remember count each time it's invoked. That's why count is increment from 0 to 3

`node src/high-order-function.js` to try
