# Pure and Impure functions

Pure functions are any functions whose output is solely derived from its inputs and causes no side effects in the application or outside world. Mathematical functions are examples of pure functions.

Impure functions come in a variety of shapes and sizes. Some typical examples would be:

- Functions whose output depends on outside/global state
- Functions that return different outputs for the same inputs
- Functions that modify application state
- Functions that modify the "outside world"

Functional programming is built upon the use of pure functions and the strict control of side effects. Being able to recognize either type of function is a key fundamental for functional programming.

```javascript
// pure
const f = x => x + 1;

// impure

// Ex 1 - Global State dependancy
const COST_OF_ITEM = 19;
const cartTotal = quantity => COST_OF_ITEM * quantity;

// Ex 2 - Same input, different output
const generateID = () => Math.floor(Math.random() * 10000);

const createUser = (name, age) => ({
  id: generateID(),
  name,
  age
});

console.log(createUser('Kyle', 33));
console.log(createUser('Kyle', 33));

// Ex. 3 - Side Effects #1
let id = 0;
const createFoodItem = name => ({
  id: ++id,
  name
});

console.log(createFoodItem('Cheeseburgers'));
console.log(createFoodItem('Fries'));
console.log(createFoodItem('Milkshakes'));
console.log(id);

// We'll see that the id increments like we expect, but if we also log out the id value itself,
// it has been mutated and is a different value.

// Ex. 4 - Side Effects #2 - Outside World

const logger = msg => {
  console.log(msg)
}

logger('Hi eggheads')

/** Every time I use console.log, it affects the terminal. That's a side effect.
 * If I have any function that uses console.log in it, such as a logger function that takes a message and outputs it,
 * that's an impure function as well, even if it's just a nice message to say, "Hi, eggheads."
 * /
```
