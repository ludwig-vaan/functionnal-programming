# immutable data structure

Mutable data structures can be changed after creations, and immutable ones cannot. Mutations can be thought of as side effects in our applications.

## mutable exemple

```javascript
const a = [1, 2, 3];
const b = a;

console.log(a === b);
// true

const a = [1, 2, 3];
const b = a;

b.push(4);
console.log(a);
// [1, 2, 3, 4]
```

## immutable exemple

```javascript
const a = [1, 2, 3];
const b = a;

console.log(a === b);
// true

const a = [1, 2, 3];
const b = [...a];

b.push(4);
console.log(a);
// [1, 2, 3]
console.log(b);
// [1, 2, 3, 4]
```
