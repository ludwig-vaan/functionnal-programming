# debug functional compositions

Functional compositions are purposely opaque, with no obvious way to "visualize" the data as it transforms. This is great when our function works, as it's hard to add bugs, but challenging when our compositions aren't correct.

Debug functional compositions by writing and using a trace() function to log out our values as they transform. This allows us to continue using pointfree programming while also providing a view into our compositions.

```javascript
const bookTitles = [
  'The Culture Code',
  'Designing Your Life',
  'Algorithms to Live By'
];

const trace = msg => x => (console.log(msg, x), x);

const slugify = compose(
  join('-'),
  trace('after lowercase'),
  map(lowerCase),
  trace('after split'),
  map(split(' ')),
  trace('before split')
);

const slugs = slugify(bookTitles);

console.log(slugs);

// SO
const slugify = map(
  compose(
    join('-'),
    split(' '),
    lowerCase
  )
);
```
