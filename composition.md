# Composition & Pipe

Composition, the building up of complex functionality through the combining of simpler functions. In a sense, composition is the nesting of functions, passing the result of one in as the input into the next. But rather than create an indecipherable amount of nesting, we'll create a higher order function, compose(), that takes all of the functions we want to combine, and returns us a new function to use in our app.

```javascript
const scream = str => str.toUpperCase();
const exclaim = str => `${str}!`;
const repeat = str => `${str} ${str}`;

console.log(repeat(exclaim(scream('I love egghead'))));
// I LOVE EGGHEAD! I LOVE EGGHEAD!

const compose = (...fns) => x => fns.reduceRight((acc, fn) => fn(acc), x);

const withExuberance = compose(
  repeat,
  exclaim,
  scream
);

console.log(withExuberance('I love egghead'));
// I LOVE EGGHEAD! I LOVE EGGHEAD!
const pipe = (...fns) => x => fns.reduceRight((acc, fn) => fn(acc), x);

const withExuberancePipe = pipe(
  scream,
  exclaim,
  repeat
);

console.log(withExuberancePipe('I love egghead'));
// I LOVE EGGHEAD! I LOVE EGGHEAD!
```
