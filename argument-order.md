# Argument order

Order the arguments of your functions following functional programming principles. By ordering our arguments in a specific way, we allow our functions to benefit from partial application, improve reusability, and enable composition of functions. Specifically, we will focus on always providing the data as the last argument to our functions so that the result of one function can be piped as the argument into another function.

```javascript
// wrong
const map = array => cb => array.map(cb);

const arr = [1, 2, 3, 4, 5];
const double = n => n * 2;

const withArr = map(arr);

console.log(withArr(double));
// [ 2, 4, 6, 8, 10 ]
console.log(withArr(n => n * 3));
// [ 3 , 6, 9, 12, 15 ]

// The only thing we can change is the callback.
```

If we change the argument order to receive the callback first and then the array second, we derive much more utility from this curried function.

```javascript
const map = cb => array => array.map(cb);

const arr = [1, 2, 3, 4, 5];
const double = n => n * 2;

const withDouble = map(double);

console.log(withDouble(arr));
// [ 2, 4, 6, 8, 10 ]
```

A useful way to think about curried functions is to order arguments from most specific to least specific argument. The least specific argument in every case is always going to be the data that could be our Boolean, a number, a string, an object, or an array, the primitives of your language.

```javascript
const prop = key => obj => obj[key];

const propName = prop('name');

const people = [
  { name: 'Jamon' },
  { name: 'Shirley' },
  { name: 'Kent' },
  { name: 'Sarah' },
  { name: 'Ken' }
];

const getNames = map(propName);
console.log(getNames(people));
```
