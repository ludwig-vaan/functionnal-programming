# partial application

how arguments passed to a curried function allow us to store data in closure to be reused in our programs and applications. Since each argument, except for the final one, returns a new function, we can easily create reusable functions by supplying some of these arguments beforehand, and sharing these partially applied functions with other parts of our codebase.

create a curried function to fetch requests from an API that uses partial application to create reusable functionality.

```javascript
// Partial Application

const fetch = require('node-fetch');

const getFromAPI = baseURL => endpoint => cb;
fetch(`${baseURL}${endpoint}`)
  .then(res => res.json())
  .then(data => cb(data))
  .catch(err => {
    console.error(err.message);
  });

const getGithub = getFromAPI('https://api.github.com');

const getGithubUsers = getGithub('/users'); 
// wait for the call back function to fetch data
const getGithubRepos = getGithub('/respositories');
// wait for the call back function to fetch data

getGithubUsers(data => {
  console.log(data.map(user => user.login))
})

getGithubUsers(data => {
  console.log(data.map(user => user.avatar_url))
})
```
